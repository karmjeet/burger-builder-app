import React from 'react';
import Aux from '../../HOC/Aux';
import Burger from '../../Components/Burger/Burger';

class BurgerBuilder extends React.Component {
   state ={
     ingredients : {
       Salad:0,
       Meat:0,
       Bacon:0,
       Cheese:0
     }
   }
  render(){
    return(
       <Aux>
         <Burger ingredients = {this.state.ingredients}/>
       </Aux>
    );
  }

}


export default BurgerBuilder;
