/**
this  is the burger container which contains all the burger visual part or subcomponents
**/
import React from "react";

import BurgerIngredients from './Burgeringredients/BurgerIngredients';

import classes from './Burger.css';

 const Burger = (props) => {

  let transformedingresient = Object.keys(props.ingredients)
                              .map(igKey => {
                                return [...Array(props.ingredients[igKey])].map((_,i) => {
                                       return <BurgerIngredients type={igKey}/>
                                } );
                              }).reduce((arr, el)=>{
                                return  arr.concat(el);
                              },[]);

                            console.log(transformedingresient.length);
   if (transformedingresient.length === 0) {
     transformedingresient = <p>please fill in the ingredients</p>
   }
   // this is the alternative approach where we used for of and entries of array method ...was thought by Me(karm)
  //  let ca = [];
  // for(const [ingredient, quantity] of Object.entries(props.ingredients)){
  //     for( var i=0;i<=quantity ; i++){
  //
  //       ca.push(<BurgerIngredients key ={ingredient,quantity} type ={ingredient}/>)
  //     }
  //   }



   return (
     <div className={classes.Burger}>
      <BurgerIngredients type="BreadTop" />
    {transformedingresient}
      <BurgerIngredients type="BreadBottom" />
     </div>
   );
   }

   export default Burger;
