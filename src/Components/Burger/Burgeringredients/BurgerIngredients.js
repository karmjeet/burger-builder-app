/**
*this is the burger ingredients components which takes the type of burger it wants to render and based on that it generates the JSX

**/

import React from "react";
import classes from "./Ingredients.css"

 class BurgerIngredients extends React.Component {
     render(){
       let ingredients = null;

      switch (this.props.type) {
        case "BreadBottom":
          ingredients = <div className={classes.BreadBottom}></div>
          break;
        case "BreadTop":
          ingredients = <div className={classes.BreadTop}>
                          <div className={classes.Seeds1}></div>
                          <div className={classes.Seeds2}></div>
                        </div>
        break;
      case "Meat":
        ingredients = <div className={classes.Meat}></div>

        break;
        case "Bacon":
          ingredients = <div className={classes.Bacon}></div>

          break;
        case "Salad":
          ingredients = <div className={classes.Salad}></div>

        break;
        case "Cheese":
          ingredients = <div className={classes.Cheese}></div>

        break;
        default:
         ingredients = null;

      }
      return ingredients;
     }
   }

   export default BurgerIngredients ;
